(defpackage mex/tests/main
  (:use :cl
        :mex
        :rove))
(in-package :mex/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :mex)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
