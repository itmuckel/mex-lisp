(in-package :mex)

(named-readtables:in-readtable rutilsx-readtable)

(defparameter *state* nil)
(defparameter *default-state* #h(:spaceship-acceleration 0.2
                                 :max-speed 16.0
                                 :inc-speed-p nil
                                 :dec-speed-p nil
                                 :rotate-delta 0.0
                                 :player-dead-p nil
                                 :init-timeout-millis 3000
                                 :game-start-time (get-millis)
                                 :spaceship nil
                                 :treasure nil
                                 :num-asteroids 400
                                 :tracked-entity nil))

(defun setup-level ()
  (setf *state* (copy-hash-table *default-state*))

  (clear-components)

  (symbol-macrolet ((spaceship (? *state* :spaceship)))
    (setf spaceship (create-entity))
    (set-component (make-c-position :xy (gk:vec2 100 -390)) :for spaceship)
    (set-component (make-c-velocity) :for spaceship)
    (set-component (make-c-appearance :image-keys (list :spaceship)) :for spaceship)

    (set-component (make-c-collision :collision-shapes
                                     (list (make-instance 'box
                                                          :width (gk:x *spaceship-size*)
                                                          :height (gk:y *spaceship-size*))))
                   :for spaceship)
    (setf (? *state* :tracked-entity) spaceship))

  (generate-treasure (gk:vec2 0 0) :player-1)
  (generate-treasure (gk:vec2 300 500) :player-1)

  (generate-asteroids))

(defun random-asteroid-image ()
  (? (list :ast1 :ast2 :ast3 :ast4 :ast5) (random 5)))

(defun generate-asteroids ()
  (dotimes (i (? *state* :num-asteroids))
    (let ((a (create-entity))
          (asteroid-img (random-asteroid-image)))
      (set-component (make-c-appearance :image-keys (list asteroid-img)) :for a)
      (set-component (make-c-position :xy (gk:vec2 (random-interval -8000 8000)
                                                   (random-interval -8000 8000))) :for a)
      (set-component (make-c-velocity :x (random-interval -1.0 1.0)
                                      :y (random-interval -1.0 1.0)) :for a)
      (set-component (make-c-collision :collision-shapes (generate-asteroid-collidables asteroid-img)) :for a))))

(defun generate-asteroid-collidables (image-key)
  "Generate multiple smaller collidables that cover the asteroid's area."

  (let* ((img-width (gk:image-width image-key))
         (img-height (gk:image-height image-key))
         (horizontal-p (> img-width img-height)))

    (if horizontal-p
      (list
       ;; left circle
       (make-instance 'circle
                      :radius (/ img-height 2.0)
                      :offset (gk:vec2 (/ img-height 2.0)
                                       (/ img-height 2.0)))
       ;; right circle
       (make-instance 'circle
                      :radius (/ img-height 2.0)
                      :offset (gk:vec2 (- img-width (/ img-height 2.0))
                                       (/ img-height 2.0)))
       ;; middle circle
       (make-instance 'circle
                      :radius (/ img-height 2.0)
                      :offset (gk:vec2 (/ img-width 2.0)
                                       (/ img-height 2.0))))
      ;; vertical-p
      (list
       ;; upper circle
       (make-instance 'circle
                      :radius (/ img-width 2.0)
                      :offset (gk:vec2 (/ img-width 2.0)
                                       (/ img-width 2.0)))
       ;; lower circle
       (make-instance 'circle
                      :radius (/ img-width 2.0)
                      :offset (gk:vec2 (/ img-width 2.0)
                                       (- img-height (/ img-width 2.0))))
       ;; middle circle
       (make-instance 'circle
                      :radius (/ img-width 2.0)
                      :offset (gk:vec2 (/ img-width 2.0)
                                       (/ img-height 2.0)))))))

(defun generate-treasure (position player)
  (with ((treasure (create-entity)))
    (set-component (make-c-collectible :value 100 :player player) :for treasure)
    (set-component (make-c-position :xy position) :for treasure)
    (set-component (make-c-appearance :image-keys (list :treasure)
                                      :animation (create-treasure-animation)) :for treasure)))

(defun get-relative-marker-position (position appearance)
  (with ((treasure-dimensions (-> (c-appearance-animation appearance)
                                  (animation-info-sprite-dimensions %)))
         (marker-position     (get-absolute-marker-position position treasure-dimensions)))

    (when (is-on-screen marker-position (gk:vec2 10 5))
      (return-from get-relative-marker-position (with-tracked-entity-offset marker-position)))

    (with ((ship-position    (gk:add (prop c-position xy (? *state* :spaceship))
                                     (gk:vec2 *canvas-center-x* *canvas-center-y*)))
           (marker-direction (gk:subt marker-position
                                      ship-position)))
      (center-to-screen-edge-intersection marker-direction :margin-x 10 :margin-y 10))))

(defun get-absolute-marker-position (treasure-position treasure-dimensions)
  (-> (c-position-xy treasure-position)
      (gk:add % (gk:vec2 (/ (gk:x treasure-dimensions) 2)
                         (gk:y treasure-dimensions)))))

(defun center-to-screen-edge-intersection (slope-delta &key (margin-x 0) (margin-y 0))
  "Given the slope of a line going from the center of the screen in any direction, this function returns the intersection of this line with an edge of the screen."
  (with ((screen-edge (which-screen-edge-gets-cut slope-delta))
         (slope-x (gk:x slope-delta))
         (slope-y (gk:y slope-delta))
         (m (/ slope-y slope-x)))
    ;; Use y/m=x and y=m*x to calculate the missing coordinate by inserting the
    ;; known coordinate in the equation.
    ;; While doing that add half of the screen to every coordinate to get
    ;; useful results.
    (case screen-edge
           (:top
            (gk:vec2 (+ (/ *canvas-center-y* m)
                        *canvas-center-x*)
                     (- (* *canvas-center-y* 2) margin-y)))
           (:bottom
            (gk:vec2 (+ (/ (- *canvas-center-y*) m)
                        *canvas-center-x*)
                     0))
           (:left
            (gk:vec2 (+ 0 margin-x)
                     (+ (* (- *canvas-center-x*) m)
                        *canvas-center-y*)))
           (:right
            (gk:vec2 (+ (* *canvas-center-x* 2) (- margin-x))
                     (+ (* *canvas-center-x* m)
                        *canvas-center-y*))))))

(defun which-screen-edge-gets-cut (slope-delta)
  (with ((slope-x (gk:x slope-delta))
         (slope-y (gk:y slope-delta))
         (slope (/ slope-y slope-x)))
    (cond ((and (plusp slope-x)
                (plusp slope-y))
           (if (> slope
                  (/ *canvas-center-y* *canvas-center-x*))
             :top
             :right))
          ((and (plusp slope-x)
                (minusp slope-y))
           (if (> slope
                  (/ (- *canvas-center-y*) *canvas-center-x*))
             :right
             :bottom))
          ((and (minusp slope-x)
                (plusp slope-y))
           (if (> slope
                  (/ *canvas-center-y* (- *canvas-center-x*)))
             :left
             :top))
          ((and (minusp slope-x)
                (minusp slope-y))
           (if (> slope
                  (/ (- *canvas-center-y*) (- *canvas-center-x*)))
             :bottom
             :left)))))

(defun debug-output (text &optional (pos (gk:vec2 200 200)))
  (gk:draw-text (format nil "~a" text)
                pos
                :fill-color *ui-color*))
(defun debug-vec (vec)
  (gk:draw-text (format nil "(~d, ~d)" (gk:x vec) (gk:y vec))
                (gk:vec2 200 100)
                :fill-color *ui-color*)
  vec)

(defun is-on-screen (pos &optional (dimensions (gk:vec2 0 0)))
  (with ((pos (with-tracked-entity-offset pos))
         (x (gk:x pos))
         (y (gk:y pos))
         (width (gk:x dimensions))
         (height (gk:y dimensions)))
    (and (> x width)
         (< x (- *canvas-width* width))
         (> y 0)
         (< y (- *canvas-height* height)))))
