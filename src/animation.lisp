(in-package :mex)

(defclass/std animation-info ()
  ((sprite-dimensions :with)
   (num-frames :with)
   (rows :with)
   (cols :with)
   (playback-speed :with)
   (should-loop :with)
   (current-frame :with)
   (time-of-start :with)
   (was-played :with)))

(defclass/std tex-src ()
  ((x :with)
   (y :with)
   (width :with)
   (height :with)))

(defmethod get-texture-origin-rect (anim image-keys)
  ;; boiler-plate :-(
  (with-accessors ((sprite-dimensions animation-info-sprite-dimensions)
                   (rows animation-info-rows)
                   (cols animation-info-cols)
                   (num-frames animation-info-num-frames)
                   (playback-speed animation-info-playback-speed)
                   (should-loop animation-info-should-loop)
                   (current-frame animation-info-current-frame)
                   (time-of-start animation-info-time-of-start)
                   (was-played animation-info-was-played))
    anim

    (when (and was-played
               (not should-loop))
      ;; early return!
      (return-from get-texture-origin-rect
                   (list (make-instance 'box :x 0 :y 0 :width 0 :height 0)
                         (first image-keys))))

    (if time-of-start
      (let* ((time-elapsed (- (get-millis) time-of-start))
             (real-frame-num (mod (round (/ time-elapsed playback-speed)) num-frames))
             (next-frame (mod real-frame-num (* rows cols))))
        (when (and (not should-loop)
                   (eql real-frame-num (- num-frames 1)))
          (setf was-played t))
        (setf current-frame (list real-frame-num next-frame)))
      (setf time-of-start (get-millis)))

    (let* ((col (truncate (mod (second current-frame) cols)))
           (row (truncate (/ (second current-frame) cols)))
           (img-num (truncate (/ (first current-frame) (* rows cols))))
           (image (? image-keys img-num)))
      (list (make-instance 'box
                           :x (* (gk:x sprite-dimensions) col)
                           ;; y is up, also for images. (+ row 1), because the box-height
                           ;; will also expand up. :facepalm:
                           :y (- (gk:image-height image) (* (gk:y sprite-dimensions) (+ row 1)))
                           :width (gk:x sprite-dimensions)
                           :height (gk:y sprite-dimensions)) image))))

(defun create-explosion-animation ()
  (make-instance 'animation-info
                           :sprite-dimensions (gk:vec2 460 259)
                           :rows 6
                           :cols 6
                           :current-frame (list 0 0)
                           :num-frames 36
                           :should-loop nil
                           :playback-speed 50
                           :time-of-start nil
                           :was-played nil))

(defun create-treasure-animation ()
  (make-instance 'animation-info
                           :sprite-dimensions (gk:vec2 200 199)
                           :rows 9
                           :cols 10
                           :current-frame (list 0 0)
                           :num-frames 90
                           :should-loop t
                           :playback-speed 30
                           :time-of-start nil
                           :was-played nil))
