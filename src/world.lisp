(in-package :mex/world)

(defparameter *components* (make-hash-table))
(defparameter *num-entities* 0)

(defun create-entity ()
  ;; add new slot for each component.
  (maphash #'(lambda (k v)
                     (setf (? *components* k)
                    		     (append v (list nil))))
       	   *components*)
  (incf *num-entities*)
  (1- *num-entities*))

(defun clear-components ()
  (setf *components* (make-hash-table))
  (setf *num-entities* 0))

(defun get-all-components (type)
  (? *components* type))

(defun get-component (type &key for)
  (? *components* type for))

(defun set-component (component &key for)
  (let ((c-type (type-of component)))
    (when (not (? *components* c-type))
      ;; TODO: Use arrays/vectors instead of lists to hold the components. :rolling_eyes:
      (setf (? *components* c-type) (make-list *num-entities* :initial-element nil)))
    (setf (? *components* c-type for) component)))

(defun get-entity-for-component (component)
  "Returns the entity the component belongs to. Currently this is just an index into the component arrays."
  (with ((c-type (type-of component))
         (components (? *components* c-type)))
    (position component components)))

(defmacro prop (component member entity)
  (let ((accessor-name (intern (concatenate 'string
                                   					    (symbol-name component)
                                   					    "-"
                                   					    (symbol-name member))
                     			       (symbol-package component))))
    `(,accessor-name (get-component ',component :for ,entity))))

(defmacro run-system (((&rest c-list) &key exclude) &body body)
  "This macro iterates through all entities for which all components from c-list are defined. The body is executed once for each component. c-list is a list of pairs '(var-name 'component)'. Thanks @zulu.inuoe. :exclude can take an entity-id that will be excluded from the system."
  (with-gensyms (zipped-list props)
    (let* ((var-names (mapcar #'first c-list))
           (clist-names (mapcar (lambda (name)
                                        (gensym (format nil "~A-COMPS" (symbol-name name))))
                                var-names)))
      (labels ((get-components
                (clist-name comp-type)
                `(,clist-name
                  (if ,exclude
                    (remove-nth (get-all-components ,comp-type) ,exclude)
                    (get-all-components ,comp-type)))))
              `(let ,(mapcar #'get-components
                             clist-names
                             (mapcar #'second c-list))
                 ; remove all entities which don't have the queried components.
                 (let ((,zipped-list (remove ()
                                             (mapcar (lambda (,@var-names)
                                                             (if (and ,@var-names)
                                                               (list ,@var-names)
                                                               ()))
                                                     ,@clist-names))))
                   (dolist (,props ,zipped-list)
                     (symbol-macrolet
                       ,(mapcar (lambda (var pos)
                                        `(,var (elt ,props ,pos)))
                                var-names
                                (loop :for i :from 0 :below (length var-names)
                                  :collect i))
                       ,@body))))))))

;;; TESTING

(defstruct c-position (x 0) (y 0) (angle 0.0))
(defstruct c-velocity (x 0.0) (y 0.0))
(defstruct c-appearance image-key)

(defvar *test-pos* (make-c-position :x 2 :y 3))

(defun init-with-values ()
  (let ((entity-id (create-entity)))
    (set-component (make-c-position :x 2 :y 3) :for entity-id)
    (format t "~a~%" *components*)))

;; (init-with-values)
