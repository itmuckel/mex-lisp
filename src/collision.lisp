(in-package :mex)

(defclass/std shape ()
  ((position :std (make-c-position) :with)))

(defclass/std box (shape)
  ((x :std 0 :with)
   (y :std 0 :with)
   (width :with)
   (height :with)))

(defclass/std circle (shape)
  ((radius :with)
   (offset :with)))

(defgeneric collide-p (pos1 shape1 pos2 shape2 &optional rotation))

(defmethod collide-p (pos1 (c1 circle) pos2 (c2 circle) &optional (rotation 0))
  (let ((distance (sqrt (distance-squared pos1 pos2)))
        (radius1 (circle-radius c1))
        (radius2 (circle-radius c2)))
    (if (<= distance (+ radius1 radius2))
      t nil)))

(defmethod collide-p (pos1 (b box) pos2 (c circle) &optional (box-rotation 0))
  (collide-p pos2 c pos1 b box-rotation))

(defmethod collide-p (pos1 (c circle) pos2 (b box) &optional (box-rotation 0))
  (circle-box-collide-axis-aligned-p pos1 c pos2 b box-rotation))

(defun circle-box-collide-axis-aligned-p (pos1 c pos2 b box-rotation)
  (let* ((radius (circle-radius c))
         (width (box-width b))
         (height (box-height b))
         (box-center (gk:vec2 (+ (gk:x pos2) (* 0.5 width))
                              (+ (gk:y pos2) (* 0.5 height))))
         (pos1-rotated (gk:add box-center
                       (rotate-vec (gk:subt pos1 box-center) box-rotation))))

    ;; find closest point on the box related to the circle
    (let ((c_x (gk:x pos1-rotated))
          (c_y (gk:y pos1-rotated))
          (b_x (gk:x pos2))
          (b_y (gk:y pos2)))

      (let ((closest_x
             (cond ((< c_x b_x) b_x)
                   ((> c_x (+ b_x width)) (+ b_x width))
                   ;; c_x is inside the box, so the nearest point is the point itself.
                   (t c_x)))
            (closest_y
             (cond ((< c_y b_y) b_y)
                   ((> c_y (+ b_y height)) (+ b_y height))
                   (t c_y))))

        ;; if the closest point on the box is inside of the circle, there's a collision.
        (< (distance-squared pos1-rotated (gk:vec2 closest_x closest_y))
           (expt radius 2.0))))))
