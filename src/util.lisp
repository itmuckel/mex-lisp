(in-package :mex/util)

(defparameter *pi-d-180* (/ pi 180.0))

(defun degrees-to-radians (d)
  (* *pi-d-180* d))

(defun radians-to-degrees (r)
  (/ r *pi-d-180*))

(defun distance-squared (vec1 vec2)
  (let ((x1 (gk:x vec1))
        (y1 (gk:y vec1))
        (x2 (gk:x vec2))
        (y2 (gk:y vec2)))
    (+ (expt (- x2 x1) 2.0)
       (expt (- y2 y1) 2.0))))

(defun left-normal (vec)
  "Given a direction vector returns the normal pointing to the left of it."
  (gk:vec2 (- (gk:y vec))
           (gk:x vec)))

(defun right-normal (vec)
  "Given a direction vector returns the normal pointing to the right of it."
  (gk:vec2 (gk:y vec)
           (- (gk:x vec))))

(defun rotate-vec (vec degrees)
  (let ((x (gk:x vec))
        (y (gk:y vec))
        (radians (degrees-to-radians degrees)))
    (gk:vec2 (- (* x (cos radians))
                (* y (sin radians)))
             (+ (* x (sin radians))
                (* y (cos radians))))))

(defun vec-len-squared (vec-x vec-y)
  (+ (expt vec-x 2.0) (expt vec-y 2.0)))

(defun vec-len (vec)
  (sqrt (vec-len-squared (gk:x vec) (gk:y vec))))

(defun get-millis ()
  "Return milliseconds since certain point of time"
  (get-internal-real-time))

(defun remove-nth (lst nth)
  (remove-if (constantly t) lst :start nth :count 1))

(defun random-interval (lower upper)
  (let ((range (+ upper (- lower))))
    (- (random range) (- lower))))
