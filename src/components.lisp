(in-package :mex)

(defstruct c-position (xy (gk:vec2 0.0 0.0)) (angle 0.0))
(defstruct c-velocity (x 0.0) (y 0.0))
(defstruct c-appearance image-keys (animation nil))
(defstruct c-collision
  "contains a list of collision-shapes"
  collision-shapes)
(defstruct c-collectible value player (picked-up-p nil))
(defstruct c-ui-element draw-function)
