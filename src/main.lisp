(in-package :mex)

;; logical resolution -- fixed.
(defparameter *canvas-width* 1920)
(defparameter *canvas-height* 1080)
;; real resolution -- variable.
(defparameter *viewport-width* 1920)
(defparameter *viewport-height* 1080)

(defparameter *ui-color* (gk:vec4 0.5 1 0.5 1))

(defparameter *canvas-center-x* (/ *canvas-width* 2))
(defparameter *canvas-center-y* (/ *canvas-height* 2))

(defparameter *spaceship-size* nil)

(gk:register-resource-package :keyword (asdf:system-relative-pathname :mex "assets/"))

(gk:define-image :space-background "space-background.png")
(gk:define-image :spaceship "spaceship.png")
(gk:define-image :explosion "explosion.png")
(gk:define-image :treasure "treasure.png")

(gk:define-image :ast1 "ast1.png")
(gk:define-image :ast2 "ast2.png")
(gk:define-image :ast3 "ast3.png")
(gk:define-image :ast4 "ast4.png")
(gk:define-image :ast5 "ast5.png")

(defun reset-game ()
  (defparameter *init-timeout-millis* 3000
    "How long the player stays invincible after game start.")
  (defparameter *game-start-time* (get-millis))

  (setup-level))

(gk:defgame mex () ()
            (:canvas-width *canvas-width*)
            (:canvas-height *canvas-height*)
            (:viewport-width *viewport-width*)
            (:viewport-height *viewport-height*)
            (:draw-rate 60)
            (:act-rate 60) ; (:fullscreen-p t)
            (:viewport-title "mex"))

(defmethod gk:post-initialize ((app mex))
  (setf *spaceship-size* (gk:vec2 (gk:image-width :spaceship)
                                  (gk:image-height :spaceship)))
  (reset-game)


  (gk:bind-button :escape :pressed
                  (lambda ()
                          (gk:stop)))

  (symbol-macrolet ((rotate-delta (? *state* :rotate-delta))
                    (inc-speed-p (? *state* :inc-speed-p))
                    (dec-speed-p (? *state* :dec-speed-p)))

    (gk:bind-button :left :pressed
                    (lambda ()
                            (setf rotate-delta (- 3.0))))
    (gk:bind-button :left :released
                    (lambda ()
                            (when (< rotate-delta 0)
                              (setf rotate-delta 0))))
    (gk:bind-button :right :pressed
                    (lambda ()
                            (setf rotate-delta 3.0)))
    (gk:bind-button :right :released
                    (lambda ()
                            (when (> rotate-delta 0)
                              (setf rotate-delta 0))))
    (gk:bind-button :up :pressed
                    (lambda ()
                            (setf inc-speed-p t)
                            (setf dec-speed-p nil)))
    (gk:bind-button :up :released
                    (lambda ()
                            (setf inc-speed-p nil)))
    (gk:bind-button :down :pressed
                    (lambda ()
                            (setf dec-speed-p t)
                            (setf inc-speed-p nil)))
    (gk:bind-button :down :released
                    (lambda ()
                            (setf dec-speed-p nil))))

  (gk:bind-button :r :pressed
                  (lambda () (reset-game))))

(defmethod gk:act ((app mex))
  (unless (? *state* :player-dead-p)
    (process-ship-controls))

  (move-system)

  (check-and-resolve-collisions))

(defun check-and-resolve-collisions ()
  (when (and (player-materialized-p)
             (not (? *state* :player-dead-p)))
    (with ((collision-entity (player-collides)))
      (when (null collision-entity)
        (return-from check-and-resolve-collisions nil))

      (setf (? *state* :player-dead-p) t)
      (setf (prop c-velocity x (? *state* :spaceship)) 0)
      (setf (prop c-velocity y (? *state* :spaceship)) 0)

      ;; let ship explode! :-)
      (set-component (make-c-appearance :image-keys (list :explosion)
                                        :animation (create-explosion-animation))
                     :for (? *state* :spaceship)))))

(defun player-materialized-p ()
  (if *init-timeout-millis*
    (progn
     (let ((elapsed (- (get-millis) *game-start-time*)))
       (when (> elapsed *init-timeout-millis*)
         (setf *init-timeout-millis* nil))))
    t))

(defun spaceship-pos-corrected-by-screen-coordinates ()
  (with ((spaceship-pos (prop c-position xy (? *state* :spaceship))))
    (gk:add spaceship-pos
            (gk:vec2 (- *canvas-center-x*
                        (/ (gk:x *spaceship-size*) 2.0))
                     (- *canvas-center-y*
                        (/ (gk:y *spaceship-size*) 2.0))))))

(defun player-collides ()
  "Checks if the player collides with an entity and returns the entity the player collides with or nil otherwise."
  (let ((spaceship-pos (spaceship-pos-corrected-by-screen-coordinates))
        (spaceship-col (first (prop c-collision collision-shapes (? *state* :spaceship))))
        (spaceship-angle (prop c-position angle (? *state* :spaceship))))
    (run-system (((pos 'c-position)
                  (col 'c-collision))
                 :exclude (? *state* :spaceship))
      (let ((shapes (c-collision-collision-shapes col)))
        (dolist (s shapes)
          (when (cond ((typep s 'circle) (collide-p spaceship-pos spaceship-col
                                                    (gk:add (c-position-xy pos)
                                                            (circle-offset s))
                                                    s spaceship-angle))
                      ((typep s 'box) (collide-p spaceship-pos spaceship-col pos s)))
            (return-from player-collides (get-entity-for-component pos))))))))

(defun process-ship-controls ()
  (symbol-macrolet ((spaceship-angle (prop c-position angle (? *state* :spaceship)))
                    (spaceship-velocity-x (prop c-velocity x (? *state* :spaceship)))
                    (spaceship-velocity-y (prop c-velocity y (? *state* :spaceship)))
                    (rotate-delta (? *state* :rotate-delta))
                    (spaceship-acceleration (? *state* :spaceship-acceleration))
                    (max-speed (? *state* :max-speed)))

    (when (not (= rotate-delta 0.0))
      (setf spaceship-angle
            (mod (+ spaceship-angle rotate-delta) 360.0)))

    (when (? *state* :inc-speed-p)
      (let ((angle (degrees-to-radians spaceship-angle))
            ;; not really the vector length,
            ;; but more efficient to sqrt it, when needed.
            (vec-len (vec-len-squared spaceship-velocity-x spaceship-velocity-y)))
        (incf spaceship-velocity-x (* (sin angle) spaceship-acceleration))
        (incf spaceship-velocity-y (* (cos angle) spaceship-acceleration))

        (when (> vec-len (expt max-speed 2.0))
          (let* ((vec-len (sqrt vec-len))
                 (speed-cap (/ max-speed vec-len)))

            (setf spaceship-velocity-x (* spaceship-velocity-x speed-cap))
            (setf spaceship-velocity-y (* spaceship-velocity-y speed-cap))))))

    (when (? *state* :dec-speed-p)
      (let ((backburner 0.2)
            (backburner-still-applicable
             (> (vec-len-squared spaceship-velocity-x spaceship-velocity-y) 0.3)))
        (if backburner-still-applicable
          (progn
           (incf spaceship-velocity-x (* (- (signum spaceship-velocity-x))
                                         backburner))
           (incf spaceship-velocity-y (* (- (signum spaceship-velocity-y))
                                         backburner)))
          (progn
           (setf spaceship-velocity-x 0.0)
           (setf spaceship-velocity-y 0.0)))))))


(defun move-system ()
  (run-system (((pos 'c-position)
                (vel 'c-velocity)))
    (symbol-macrolet ((pos-x (-> pos c-position-xy gk:x))
                      (pos-y (-> pos c-position-xy gk:y)))
      (incf pos-x (c-velocity-x vel))
      (incf pos-y (c-velocity-y vel)))))

(defun draw-collision-shapes (&optional (color (gk:vec4 0 1 1 1)))
  (run-system (((pos 'c-position)
                (col 'c-collision)))
    (let ((shapes (c-collision-collision-shapes col)))
      (dolist (s shapes)
        (cond ((typep s 'circle)
               (gk:draw-circle (with-tracked-entity-offset
                                 (gk:add (c-position-xy pos)
                                         (circle-offset s)))
                               (circle-radius s)
                               :stroke-paint color))
              ((typep s 'box)
               ;; TODO: This is box specific. Generalize!
               (gk:with-pushed-canvas
                ()
                (gk:translate-canvas *canvas-center-x* *canvas-center-y*)
                (gk:rotate-canvas (- (degrees-to-radians (c-position-angle pos))))
                (gk:draw-rect (gk:div *spaceship-size* -2)
                              (box-width s)
                              (box-height s)
                              :stroke-paint color))))))))

(defmethod gk:draw ((app mex))
  (draw-space-background)

  (draw-environment)

  (draw-space-ship)

  (draw-target-markers)

  ; (draw-collision-shapes)

  (draw-ui))

(defun draw-target-markers ()
  (run-system (((appearance 'c-appearance)
                (pos 'c-position)
                (treasure 'c-collectible)))
    (draw-target-marker pos appearance)))

(defun draw-target-marker (position appearance)
  "Draws the target marker at the treasure's position if it's on screen and on the edge of the screen otherwise."
  (with ((treasure-pos (get-relative-marker-position position appearance))
         (marker-size 10)
         (triangle-left (gk:add treasure-pos (gk:vec2 (- marker-size) marker-size)))
         (triangle-right (gk:add treasure-pos (gk:vec2 marker-size marker-size)))
         (triangle-bottom treasure-pos))
    (gamekit:draw-polygon (list triangle-left
                                triangle-right
                                triangle-bottom)
                          :fill-paint *ui-color*)))

(defun draw-space-ship ()
  (with ((spaceship (? *state* :spaceship)))
    (gk:with-pushed-canvas
     ()
     ;; translate and rotate canvas to draw the spaceship.
     (gk:translate-canvas *canvas-center-x* *canvas-center-y*)
     (let ((spaceship-angle (prop c-position angle spaceship)))
       (gk:rotate-canvas (- (degrees-to-radians spaceship-angle))))

     ;; actual drawing.
     (with ((image-keys (prop c-appearance image-keys spaceship))
            (animation (prop c-appearance animation spaceship))
            (center (if animation
                      (gk:subt (gk:vec2 0 0) (gk:div (animation-info-sprite-dimensions animation) 2))
                      (gk:subt (gk:vec2 0 0) (gk:div *spaceship-size* 2)))))
       (draw-entity image-keys animation center)))))

(defun draw-environment ()
  "Draws everything except the player's ship and the UI."
  (run-system
    (((appearance 'c-appearance)
      (pos 'c-position))
     :exclude (? *state* :spaceship))

    (with ((image-keys (c-appearance-image-keys appearance))
           (animation (c-appearance-animation appearance))
           (position (with-tracked-entity-offset (c-position-xy pos))))
      (draw-entity image-keys animation position))))

(defun draw-entity (image-keys animation position)
  (if animation
    (with (((rect img) (get-texture-origin-rect animation image-keys))
           (origin (gk:vec2 (box-x rect) (box-y rect)))
           (width (box-width rect))
           (height (box-height rect)))
      (gk:draw-image position img :origin origin :width width :height height))
    (gk:draw-image position (first image-keys))))

(defun draw-ui ()
  (when *init-timeout-millis*
    (with ((elapsed (- (get-millis) *game-start-time*))
           (init-time-left (- *init-timeout-millis* elapsed)))
      (gk:draw-text (format nil "~a" init-time-left)
                    (gk:vec2 20 50)
                    :fill-color *ui-color*)))
  (let* ((pos (prop c-position xy (? *state* :spaceship)))
         (pos-x (gk:x pos))
         (pos-y (gk:y pos)))
    (gk:draw-text (format nil "(~d, ~d)" (round pos-x) (round pos-y))
                  (gk:vec2 (- *canvas-width* 200) 100)
                  :fill-color *ui-color*)))

(defun with-tracked-entity-offset (pos)
  "Returns the object's position relative to the tracked entity.
This is useful mainly for drawing purposes."
  (let ((offset (gk:subt 0 (prop c-position xy (? *state* :tracked-entity)))))
    (gk:add pos offset)))

(defun draw-space-background ()
  ;; the texture has a width and height of 1024,
  ;; but for some reason you can see white borders at the transitions.
  ;; Probably this was true for the rust version too, but the background there
  ;; defaulted to black. (- width 1) should cover rounding mistakes.
  (let* ((space-width 1023)
         (spaceship-pos (prop c-position xy (? *state* :spaceship)))
         (base-position (gk:vec2
                         (mod (/ (- (gk:x spaceship-pos)) 2.0) space-width)
                         (mod (/ (- (gk:y spaceship-pos)) 2.0) space-width))))

    (labels ((draw-tile (x-fac y-fac)
                        (gk:draw-image (gk:vec2 (+ (gk:x base-position) (* x-fac space-width))
                                                (+ (gk:y base-position) (* y-fac space-width)))
                                       :space-background)))
            (draw-tile 0 0)
            (draw-tile 1 0)
            (draw-tile -1 0)
            (draw-tile 0 1)
            (draw-tile 0 -1)
            (draw-tile 1 1)
            (draw-tile 1 -1)
            (draw-tile -1 1)
            (draw-tile -1 -1))))


(defun run ()
  (gk:start 'mex :swap-interval 0))

(run)
