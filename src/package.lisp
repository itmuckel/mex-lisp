(in-package :cl-user)

(defpackage :mex/util
  (:use :cl
        :rutilsx)
  (:shadow :rotate)
  (:local-nicknames
   (:gk :trivial-gamekit))
  (:export :degrees-to-radians
           :radians-to-degrees
           :distance-squared
           :vec-len-squared
           :vec-len
           :get-millis
           :rotate-vec
           :random-interval
           :remove-nth))


(defpackage :mex/world
  (:use :cl
        :rutilsx
        :mex/util)
  (:export :create-entity
       	   :get-all-components
       	   :get-component
       	   :set-component
           :get-entity-for-component
           :clear-components
       	   :run-system
           :prop))

(defpackage :mex
  (:use :cl
        :rutilsx
       	:mex/world
        :mex/util)
  (:local-nicknames
   (:gk :trivial-gamekit))
  (:import-from :defclass-std :defclass/std)
  (:export :run))
