(defsystem "mex"
  :version "0.1.0"
  :author "itmuckel"
  :license ""
  :depends-on (:trivial-gamekit
               :rutilsx
               :defclass-std)
  :components ((:module "src"
                        :components
                        ((:file "package")
                         (:file "util")
                         (:file "world")
                         (:file "components")
                         (:file "collision")
                         (:file "animation")
                         (:file "level")
                         (:file "main"))))
  :description ""
  :in-order-to ((test-op (test-op "mex/tests"))))

(defsystem "mex/tests"
  :author ""
  :license ""
  :depends-on ("mex"
               "rove")
  :components ((:module "tests"
                        :components
                        ((:file "world")
                    				 (:file "main" :depends-on ("world")))))
  :description "Test system for mex"
  :perform (test-op (op c) (symbol-call :rove :run c)))
