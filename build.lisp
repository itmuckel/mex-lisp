(ql:quickload :mex)
(ql:quickload :trivial-gamekit/distribution)

(ql:quickload :uiop)
(when (uiop:directory-exists-p #p"./build/")
  (uiop:delete-directory-tree #p"./build/" :validate t))

(in-package :mex)
(trivial-gamekit.distribution:deliver "mex" 'mex)
